package com.sourav.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sourav.app.model.Product;
import com.sourav.app.repository.ProductRepository;

@RestController
@RequestMapping("products/v1")
@CrossOrigin(maxAge = 3600)
public class ProductController {
	
	@Autowired
	ProductRepository productRepo;
	
	@GetMapping("/all")
	public List<Product> getAllProducts(){
		return productRepo.findAll();
	}
	
	@PostMapping(value="/add")	
	public Product addProduct(@RequestBody Product p1) {
          
           Product p = new Product();
           p.setProductName(p1.getProductName());
           p.setProductBrand(p1.getProductBrand());
           p.setProductPrice(p1.getProductPrice());           
           return productRepo.save(p1);     
		
	}
	
	@GetMapping("/{id}")
	public Product getProductIdById(@PathVariable Long id) {
		Product px = new Product();
		System.out.println("Test "+px);
		return productRepo.findById(id).orElse(px);
	}

}