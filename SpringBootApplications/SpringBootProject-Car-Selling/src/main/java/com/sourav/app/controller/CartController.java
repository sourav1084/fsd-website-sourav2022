package com.sourav.app.controller;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sourav.app.model.Cart;
import com.sourav.app.model.Product;
import com.sourav.app.model.User;
import com.sourav.app.repository.CartRepository;
import com.sourav.app.repository.ProductRepository;
import com.sourav.app.repository.UserRepository;

@RestController
@RequestMapping("carts/v1")
@CrossOrigin(maxAge = 3600)
public class CartController {
	
	@Autowired
	private CartRepository cartRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ProductRepository productRepo;
	
	@PostMapping("/add")
	public Cart createCard() {
		Cart c = new Cart();
		cartRepo.save(c);
		return c;
	}
	
	@PostMapping("/addcartforuser/{username}")
	public User createCardForUser(@PathVariable String username) {
		User existingUser = userRepo.findById(username).orElse(null);
		
		if(existingUser !=null) {
			Cart c = new Cart();
			cartRepo.save(c);
			existingUser.setCart(c);
			userRepo.save(existingUser);
		}		
		return existingUser;
	}
	
	@PostMapping("/addproducttocart/{cartId}/{pid}")
	public User addProductToCardId(@PathVariable String cartId,@PathVariable String pid) {
		
		Cart getCartForID = cartRepo.findById(Long.valueOf(cartId)).orElse(null);
		
		if(getCartForID !=null) {
			User existingUser = userRepo.getUserByCarId(getCartForID);
			Product p = productRepo.findById(Long.valueOf(pid)).orElse(null);
			Set<Product> plist = getCartForID.getProductList();
			plist.add(p);
			getCartForID.setProductList(plist);
			cartRepo.save(getCartForID);
			return existingUser;
		}else{
			return new User();
		}
		
	}
	
	@PostMapping("/addproducttocartuser/{username}/{pid}")
	public User addProductToUserName(@PathVariable String username,@PathVariable String pid) {
		
		User existingUser = userRepo.findById(username).orElse(null);
		
		if(existingUser !=null) {
			Cart c = existingUser.getCart();
			if(c ==null) {
				c = createCard();
				existingUser.setCart(c);
				userRepo.save(existingUser);				
				c = existingUser.getCart();
			}
			Product p = productRepo.findById(Long.valueOf(pid)).orElse(null);
			Set<Product> plist = c.getProductList();
			if(plist == null) {
				plist = new LinkedHashSet<>();
			}
			plist.add(p);
			c.setProductList(plist);
			cartRepo.save(c);
			return existingUser;
		}else{
			return new User();
		}
		
	}

	@GetMapping("/all")
	public List<Cart> getAllCartItems(){
		return cartRepo.findAll();
	}
	
	@GetMapping("/itemcount")
	public String getCartItemCount(){
		return String.valueOf(cartRepo.findAll().size());
	}
	
	
	@DeleteMapping("/delallcartitems/{username}")
	public void deleteAllProductIdFromCart(@PathVariable String username) {
		User existingUser = userRepo.findById(username).orElse(null);
		if(existingUser !=null) {
			Cart c = existingUser.getCart();
			if(c ==null) {
				c = createCard();
				existingUser.setCart(c);
				userRepo.save(existingUser);				
				c = existingUser.getCart();
			}			
			c.setProductList(new LinkedHashSet<>());
			cartRepo.save(c);			
		}
	}
	
	@DeleteMapping("/delproductfromcartuser/{username}/{pid}")
	public User deleteUserIdFromCart(@PathVariable String username,@PathVariable String pid) {
		
		User existingUser = userRepo.findById(username).orElse(null);
		
		if(existingUser !=null) {
			Cart c = existingUser.getCart();
			if(c ==null) {
				c = createCard();
				existingUser.setCart(c);
				userRepo.save(existingUser);				
				c = existingUser.getCart();
			}
			Product p = productRepo.findById(Long.valueOf(pid)).orElse(null);
			Set<Product> plist = c.getProductList();
			if(plist == null) {
				plist = new LinkedHashSet<>();
			}
			plist.remove(p);
			c.setProductList(plist);
			cartRepo.save(c);
			return existingUser;
		}else{
			return new User();
		}
		
	}

}
