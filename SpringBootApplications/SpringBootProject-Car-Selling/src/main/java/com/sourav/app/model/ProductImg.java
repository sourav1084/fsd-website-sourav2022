package com.sourav.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "productimg")
public class ProductImg {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pimg_id;

    private String fileName;

    private String fileType;

    @Lob
    @JsonProperty(access = Access.WRITE_ONLY)
    private byte[] imgData;

    public ProductImg() {

    }

    public ProductImg(String fileName, String fileType, byte[] data) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.imgData = data;
    }

	public Long getId() {
		return pimg_id;
	}

	public void setId(Long id) {
		this.pimg_id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return imgData;
	}

	public void setData(byte[] data) {
		this.imgData = data;
	}

   
}
