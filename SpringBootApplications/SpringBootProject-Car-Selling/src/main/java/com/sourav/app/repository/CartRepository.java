package com.sourav.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sourav.app.model.Cart;

public interface CartRepository extends JpaRepository<Cart,Long>{
	
	@Query(value="SELECT c.id,c.user_id FROM cart c WHERE c.user_id = ?1", nativeQuery = true)
	public List<Cart> getCartByuserId(String uid);

}
