package com.sourav.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sourav.app.model.Cart;
import com.sourav.app.model.Product;
import com.sourav.app.model.User;
import com.sourav.app.repository.UserRepository;

@RestController
@RequestMapping("users/v1")
@CrossOrigin(maxAge = 3600)
public class UserController {
	
	@Autowired
	private UserRepository userRepo;
	
	@GetMapping("/all")
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}
	
	@PostMapping("/add")
	public User addUser(@RequestBody User u) {
		User existingUser = userRepo.findById(u.getUsername()).orElse(null);
		
		if(existingUser == null) {
			return userRepo.save(u);
		}else {
			throw new UsernameAlreadyExistsException("User already exists in DB");
		}
		
	}
	
	@PostMapping("/login")
	public User loginCheck(@RequestBody User u) {
		User updatedUser = userRepo.loginCheck(u.getUsername(), u.getPassword());
		
		if(updatedUser !=null) {
			return updatedUser;
		}else {
			throw new UserNotFoundException("Login Error");
		}
	}
	
	@GetMapping("/{userName}")
	public User getUserByUserName(@PathVariable String userName) {
		return userRepo.findById(userName).orElse(new User());
	}
	
	@GetMapping("/cart/{userName}")
	public Cart getCartIdByUserName(@PathVariable String userName) {
		User u = userRepo.findById(userName).orElse(new User());
		if(u.getCart()==null) {
			return new Cart();
		}
		return u.getCart();
	}
	
	@GetMapping("/cartitemcount/{userName}")
	public Integer getCartItemCount(@PathVariable String userName) {
		User u = userRepo.findById(userName).orElse(new User());
		if(u.getCart()==null) {
			return 0;
		}
		return u.getCart().getProductList().size();
	}
	
	
	@GetMapping("/cartitems/{userName}")
	public List<Product> getCartItemsByUserName(@PathVariable String userName) {
		User u = userRepo.findById(userName).orElse(new User());
		if(u.getCart()==null) {
			return (List<Product>) new Cart().getProductList();
		}
		return new ArrayList<Product>(u.getCart().getProductList());
	}
	
	
	@PutMapping("/update")
	public User updateUser(@RequestBody User u) {
		User updatedUser = userRepo.findById(u.getUsername()).orElse(null);
		
		if(updatedUser !=null) {
			updatedUser.setPassword(u.getPassword());
			updatedUser.setEmail(u.getEmail());
			updatedUser.setMobile(u.getMobile());
			updatedUser.setCart(u.getCart());
			return userRepo.save(updatedUser);
		}else {
			return new User();
		}
	}
	
	@DeleteMapping("/delete/{userName}")
	public void deleteUserName(@PathVariable String userName) {
		userRepo.deleteById(userName);
	}

}
