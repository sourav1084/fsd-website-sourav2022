package com.sourav.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class Car {

	@Id
	@Column(name = "Model_No")
	private long modelId;
	
	@Column(name = "Car_price")
	private long carPrice;
	
	@Column(name = "Car_MFGDate")
	private int mfqDate;
	
	@Column(name = "Car_Name")
	private String carName;
	
	@Column(name = "Car_Features")
	private String carFeatures;
	
	@Column(name = "Car_Color")
	private String carColor;
	
	@Column(name = "Car_Fuel(P/D)")
	private String carfulePD;

	Car() {

	}

	public Car(long modelId, long carPrice, int mfqDate, String carName, String carFeatures, String carColor,
			String carfulePD) {

		this.modelId = modelId;
		this.carPrice = carPrice;
		this.mfqDate = mfqDate;
		this.carName = carName;
		this.carFeatures = carFeatures;
		this.carColor = carColor;
		this.carfulePD = carfulePD;
	}

	public long getModelId() {
		return modelId;
	}

	public void setModelId(long modelId) {
		this.modelId = modelId;
	}

	public long getCarPrice() {
		return carPrice;
	}

	public void setCarPrice(long carPrice) {
		this.carPrice = carPrice;
	}

	public int getMfqDate() {
		return mfqDate;
	}

	public void setMfqDate(int mfqDate) {
		this.mfqDate = mfqDate;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarFeatures() {
		return carFeatures;
	}

	public void setCarFeatures(String carFeatures) {
		this.carFeatures = carFeatures;
	}

	public String getCarColor() {
		return carColor;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	public String getCarfulePD() {
		return carfulePD;
	}

	public void setCarfulePD(String carfulePD) {
		this.carfulePD = carfulePD;
	}

}
