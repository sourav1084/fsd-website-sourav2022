package com.sourav.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UsernameAlreadyExistsException extends RuntimeException {
	private static final long serialVersionUID = 8288760677888614609L;

	public UsernameAlreadyExistsException(String message) {
		super(message);
	}
}
