package com.sourav.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sourav.app.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>{

}
