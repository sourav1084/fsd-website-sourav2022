package com.sourav.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sourav.app.model.Product;
import com.sourav.app.model.ProductImg;
import com.sourav.app.service.ProductImgStorageService;

@RestController
@RequestMapping("img/v1")
@CrossOrigin(maxAge = 3600)
public class ProductImgController {

    private static final Logger logger = LoggerFactory.getLogger(ProductImgController.class);

    @Autowired
    private ProductImgStorageService dbFileStorageService;

    @PostMapping("/uploadFile/{productId}")
    public Product uploadFile(@RequestParam("file") MultipartFile file,@PathVariable Long productId ) {
        /*ProductImg dbFile = dbFileStorageService.storeFile(file,productId);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(String.valueOf(dbFile.getId()))
                .toUriString();

		
		 * return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri,
		 * file.getContentType(), file.getSize());
		 */
        
        return dbFileStorageService.storeFile(file,productId);
    }

	/*
	 * @PostMapping("/uploadMultipleFiles") public List<UploadFileResponse>
	 * uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) { return
	 * Arrays.asList(files) .stream() .map(file -> uploadFile(file))
	 * .collect(Collectors.toList()); }
	 */

    @GetMapping("/downloadFile/{fileId}")
    public UploadFileResponse downloadFile(@PathVariable Long fileId) {
        // Load file from database
        ProductImg dbFile = dbFileStorageService.getFile(fileId);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(String.valueOf(dbFile.getId()))
                .toUriString();

        return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri,
                dbFile.getFileType(), dbFile.getData().length);
    }
    
    @GetMapping("/downloadImg/{fileId}")
    public ResponseEntity<Resource> downloadImg(@PathVariable Long fileId) {
        // Load file from database
    	ProductImg dbFile = dbFileStorageService.getFile(fileId);
        // cna be used to display <img src="http://localhost:8080/downloadImg/2" width='400px' alt="testimg"/>
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }

}
