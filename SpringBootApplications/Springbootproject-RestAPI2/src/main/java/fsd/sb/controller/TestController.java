package fsd.sb.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@GetMapping("/msg")
	public String msg() {
		return "hi";
	}
	@GetMapping("/msg2")
	public String hi2someone(@PathParam(value = "name") String name) {
		return "Hi ! " +name + ":)";
	}
	@GetMapping("/msg3")
	public String hiThere(@PathParam(value = "fname") String fname,@PathParam(value = "lname")String lname){
		return "Hello "+fname+","+lname;
		
	}
	
	
}
