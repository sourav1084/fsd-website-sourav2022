package fsd.jdbc.demo;
import fsd.demo.Validations.*;
import fsd.jdbc.demo.model.User;
import fsd.jdbc.demo.service.UserServiceImpl;

public class UserLoginAppV4 {

	public static void main(String[] args) {
		
		User u1 = new User("user2","pass","user2@sd.com");
		
		UserServiceImpl userSvc = new UserServiceImpl();
		
		if(Validation.isValidUserName(u1.getUsername()) && Validation.isValidPassword(u1.getPassword())) {
			if(!userSvc.checkExistingUser(u1)) {
				System.out.println("Username doesnot exist\n Creating new username....");
				if(userSvc.addUser(u1)) {
					System.out.println("User added successfully");
				}else {
					System.out.println("Error adding user");
				}				
			}else {
				System.out.println("Username aleary exist\nPlease choose another username");
			}
		}else {
			System.out.println("Please enter valid username or password format");
		}
		
		
		//User u2 = new User();
		//userSvc.loginCheck(u2);

	}

}
