package fsd.jdbc.demo.service;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fsd.jdbc.demo.dbutil.MySQlConnection;
import fsd.jdbc.demo.model.User;

public class UserServiceImpl  {
	
	public static final String ADD_USER_SQL = "insert into user(username,password,email,mobileno) values "
			+ "(?,?,?,?);";	
	
	public static final String USER_LOGIN_SQL = "select * from user where username=? and password=?;";
	
	public static final String USER_EXIIST_SQL = "select * from user where username=?;";

	public boolean loginCheck(User u) {
		try {
			PreparedStatement preLoginStmt = MySQlConnection.getMySqlConnection()
					.prepareStatement(USER_LOGIN_SQL);
			preLoginStmt.setString(1, u.getUsername());
			preLoginStmt.setString(2, u.getPassword());

			ResultSet rs = preLoginStmt.executeQuery();
			while (rs.next()) {
				System.out.println("########Login SUCCESSFUL!! #############");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean addUser(User u) {
		try {
			PreparedStatement adduserStmt = MySQlConnection.getMySqlConnection()
					.prepareStatement(ADD_USER_SQL);
			adduserStmt.setString(1, u.getUsername());
			adduserStmt.setString(2, u.getPassword());
			adduserStmt.setString(3, u.getEmail());
			adduserStmt.setString(4, u.getMobileNo());

			int updateCounter = adduserStmt.executeUpdate();
			if(updateCounter!=0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean checkExistingUser(User u) {
		try {
			PreparedStatement checkExistingUserStmt = MySQlConnection.getMySqlConnection()
					.prepareStatement(USER_EXIIST_SQL);
			checkExistingUserStmt.setString(1, u.getUsername());

			ResultSet rs = checkExistingUserStmt.executeQuery();
			while (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}


}
