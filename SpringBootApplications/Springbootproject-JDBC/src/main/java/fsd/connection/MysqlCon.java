package fsd.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

class MysqlCon {
	public static void main(String args[]) {
		try {
			//Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/fsd", "root", "mysql"); 
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user");
			while (rs.next())
				System.out.println(rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
			
			//int insertNos = stmt.executeUpdate("insert  into user(username,password,firstname,lastname,email) values"
			//		+ " ('user3','1234','Pankaj','kumar','pankaj@gmail.com');");
			//System.out.println(insertNos);
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

