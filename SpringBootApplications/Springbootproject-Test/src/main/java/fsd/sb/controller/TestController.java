package fsd.sb.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@SuppressWarnings("deprecation")
	@GetMapping("/test")
	public String testing() {
		System.out.println("Testing.....");
		return "Todays Date is "+new Date();
	}

}
