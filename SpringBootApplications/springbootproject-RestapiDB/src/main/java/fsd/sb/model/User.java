package fsd.sb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.RowId;

@Entity
@Table
public class User implements Serializable {

	@Column
	@Id // Primary Key
	private Long userId;

	@Column
	private String uName;

	@Column
	private String email;

	public User() {
	}

	public User(Long userId, String uName, String email) {
		super();
		this.userId = userId;
		this.uName = uName;
		this.email = email;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getuName() {
		return uName;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
