package fsd.sb.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fsd.sb.model.User;

@SuppressWarnings("hiding")
@Repository
public interface UserRepository extends CrudRepository<User,Long>{

}
