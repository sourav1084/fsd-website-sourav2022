package fsd.sb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fsd.sb.model.User;
import fsd.sb.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	
	//Create Operation
	public void addUser(User u) {
		 userRepo.save(u);
	}
	
	// Retrive
	public List<User> getAllUsers(){
		return (List<User>) userRepo.findAll();
	}
	
	//Delete Operation
	public void deleteUserById(Long id) {
		userRepo.deleteById(id);
	}
	
	//Update
	public void updateUser(User u) {
		userRepo.save(u);
	}


	

}
