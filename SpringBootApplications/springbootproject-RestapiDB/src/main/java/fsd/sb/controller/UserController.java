package fsd.sb.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fsd.sb.model.User;

import fsd.sb.services.*;

@RestController
public class UserController {

	private static Logger logger = Logger.getLogger(UserController.class.getName());

	@Autowired
	private UserService userSvc;

	@PostMapping("/adduser")
	public User addUser(@RequestBody User u1) {
		// User u1= new User(100L,"user1","user1@user1.com");

		System.out.println(u1.getuName());

		userSvc.addUser(u1);

		return u1;
	}

	@GetMapping("/users")
	public List<User> showAllUser() {
		return userSvc.getAllUsers();
	}

	@PutMapping("/user/update")
	public void updateUser(@RequestBody User u) {
		userSvc.updateUser(u);
	}

	@DeleteMapping("/delete/user/{id}")
	public String deleteUserById(@PathVariable(value = "id") Long id) {
		userSvc.deleteUserById(id);
		return "user with id=" + id + " deleted";
	}

}
