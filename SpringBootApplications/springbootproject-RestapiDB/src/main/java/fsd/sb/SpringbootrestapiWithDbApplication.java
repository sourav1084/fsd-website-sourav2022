package fsd.sb;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 

@SpringBootApplication
public class SpringbootrestapiWithDbApplication {

	static Logger log = Logger.getLogger(SpringbootrestapiWithDbApplication.class.getName());
	public static void main(String[] args) {
		log.info("STARTING String Boot Application....");
		SpringApplication.run(SpringbootrestapiWithDbApplication.class, args);
		
		
	}

}

