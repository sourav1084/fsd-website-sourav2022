package com.fsd.sb.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.sb.model.Address;

@RestController
public class AddressController {
	
	@GetMapping("/test")
	public String test() {
		
		Address a1 = new Address();
		a1.setId(1002L);
		a1.setFlatNo("A123");
		a1.setStreet("1st Main");
		a1.setPincode(560085L);
		
		System.out.println(a1.toString());
		return "Test OK";
	}
	
	@GetMapping("/address")
	public Address showAddress() {
		
		Address a1 = new Address();
	    a1.setId(1002L);
		a1.setFlatNo("A123");
		a1.setStreet("Marathahalli 1st cross Road Main");
		a1.setPincode(560085L);
		
		//System.out.println(a1.getId());
		return a1;
	}

}
