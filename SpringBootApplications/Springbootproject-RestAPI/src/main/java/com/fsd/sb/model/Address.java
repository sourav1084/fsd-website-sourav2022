package com.fsd.sb.model;

public class Address {
	
	private Long id;
	private String flatNo;
	private String street;
	private Long pincode;
	
	public Address() {}
	
	public Address(Long id, String flatNo, String street, Long pincode) {
		super();
		this.id = id;
		this.flatNo = flatNo;
		this.street = street;
		this.pincode = pincode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlatNo() {
		return flatNo;
	}

	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	/*@Override
	public String toString() {
		return "Address [id=" + id + ", flatNo=" + flatNo + ", street=" + street + ", pincode=" + pincode + "]";
	}*/
	
	
   
}

