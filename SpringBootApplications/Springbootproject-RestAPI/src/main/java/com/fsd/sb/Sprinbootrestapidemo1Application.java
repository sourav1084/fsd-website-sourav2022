package com.fsd.sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sprinbootrestapidemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Sprinbootrestapidemo1Application.class, args);
	}

}
