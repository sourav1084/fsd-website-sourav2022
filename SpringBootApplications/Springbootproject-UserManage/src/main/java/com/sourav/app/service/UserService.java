package com.sourav.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sourav.app.model.User;
import com.sourav.app.repository.UserRepository;

public class UserService {

	@Autowired
	private UserRepository userRepo;

	public void addUser(User u) {

		userRepo.save(u);
		
	}
	
	public List<User> getAllUsers() {
		return (List<User>) userRepo.findAll();
		
	}
	
}
