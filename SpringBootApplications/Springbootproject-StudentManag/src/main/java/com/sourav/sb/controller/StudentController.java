package com.sourav.sb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sourav.sb.entity.Student;
import com.sourav.sb.service.impl.StudentServiceImpl;

@RestController
public class StudentController {

	@Autowired
	private StudentServiceImpl studentService;

	
	
	//handler method to handle list students and return mode and view
	
	@GetMapping("/students")
	public List<Student> listStudents() {
		System.out.println("-----"+studentService.getAllStudents());
		return studentService.getAllStudents();
		
	}
	
	@PostMapping("/add")
	public Student addStudent(@RequestBody Student s) {
		return studentService.addStudent(s);
	}
	
	
	
}
