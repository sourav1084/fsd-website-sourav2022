package ObjectOriented;

class Student1 {
	
	String name;
	int age;
	
	public void printInfo() {
		System.out.println(this.name);
		System.out.println(this.age);
	}
	
	public Student1() { //Default constructor
		
	}
	
//	Student1(String name, int age){            //constructor
//		System.out.println("Constructor Called");
//		
//		this.name =name;
//		this.age=age;
//	}
	public Student1(Student1 s2) {
		
		this.name=s2.name;
		this.age=s2.age;
	}

}
    public class Student{
    	
	public static void main(String[] args) {
		
		Student1 s1 =new Student1();
		s1.name="James";
		s1.age=21;
		
		Student1 s2 = new Student1(s1);
		
//		s1.printInfo();
		s2.printInfo();
	}

}
