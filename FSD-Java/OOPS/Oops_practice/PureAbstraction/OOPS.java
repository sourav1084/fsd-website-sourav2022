package PureAbstraction;

interface Animal {
	
	int eyes =2;
	public void walk();
	public void eyes();
}

class Horse implements Animal {

	public void walk() {
		System.out.println("5 legs");
	}

	public void eyes() {
		System.out.println("2 eyes");	
	}

}

public class OOPS {

	public static void main(String[] args) {
		
		Horse horse = new Horse();
		horse.walk();
		horse.eyes();

	}
}