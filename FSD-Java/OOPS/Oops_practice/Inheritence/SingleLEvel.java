package Inheritence;

class Shape2 { // Parent Class or Super Class

	public void area() {

		System.out.println("Displays area");
	}
}

class Triangle2 extends Shape2 { // Child Class or sub class

	private void area(int l, int h) {

		System.out.println(1 / 2 * l * h);
	}
}

public class SingleLEvel { // single level Inheritence

	public static void main(String[] args) {

	}

}
