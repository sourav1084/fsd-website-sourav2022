package Inheritence;

class Shape {// parent class or super class

	String color;
}

class Triangle extends Shape { // child class or sub class

}

public class OOPS {

	public static void main(String[] args) {

		Triangle t1 = new Triangle();

		t1.color = "red";
		System.out.println(t1.color);

	}
}
