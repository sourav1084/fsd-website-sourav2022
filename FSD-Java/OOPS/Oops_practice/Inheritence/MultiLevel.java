package Inheritence;

class Shape3 { // Parent Class or Super Class

	public void area() {

		System.out.println("Displays area");
	}
}

class Square extends Shape3 { // Child Class or sub class

	private void area(int l, int h) {

		System.out.println(1 / 2 * l * h);
	}
}

class EquilateralTriangle extends Square { // Child Class or sub class

	private void area(int l, int h) {

		System.out.println(1 / 2 * l * h);
	}
}

public class MultiLevel { // MultiLevel Inheritence

	public static void main(String[] args) {

	}

}
