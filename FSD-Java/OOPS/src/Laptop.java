
public class Laptop extends ElectronicDevice { //is a relation
	
         private Battery LapBattery;//LAP HAS A  BATTERY

		public Laptop(String string) {
			super(string);
			
		}
		/**
		 * @return the lapBattery
		 */
		public Battery getLapBattery() {
			return LapBattery;
		}

		/**
		 * @param lapBattery the lapBattery to set
		 */
		public void setLapBattery(Battery lapBattery) {
			LapBattery = lapBattery;
		}

		@Override
		public String toString() {
			return "Laptop [LapBattery=" + LapBattery + "]";
		}
		
		public static void main(String[] args) {
			
			ElectronicDevice l = new ElectronicDevice(10,"Sourav",10000,"Blue");
			l.setdColor("red");
		}
}
