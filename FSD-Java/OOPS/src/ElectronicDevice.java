public class ElectronicDevice {// ElectronicDevice extend by defalt parent class Object

	
	private int dID;
	private String dName;
	private int dPrice;
	private String dColor;
	
	public ElectronicDevice(String string) {
		
	}
	public ElectronicDevice(int dId, String dName,int dPrice,String dColor) {
		this.dID=dId;
		this.dColor =dColor;
		this.dName =dName;
		this.dPrice=dPrice;
		
		
	}
	/**
	 * @return the dID
	 */
	public int getdID() {
		return dID;
	}
	/**
	 * @param dID the dID to set
	 */
	public void setdID(int dID) {
		this.dID = dID;
	}
	/**
	 * @return the dName
	 */
	public String getdName() {
		return dName;
	}
	/**
	 * @param dName the dName to set
	 */
	public void setdName(String dName) {
		this.dName = dName;
	}
	/**
	 * @return the dPrice
	 */
	public int getdPrice() {
		return dPrice;
	}
	/**
	 * @param dPrice the dPrice to set
	 */
	public void setdPrice(int dPrice) {
		this.dPrice = dPrice;
	}
	/**
	 * @return the dColor
	 */
	public String getdColor() {
		return dColor;
	}
	/**
	 * @param dColor the dColor to set
	 */
	public void setdColor(String dColor) {
		this.dColor = dColor;
	}
	
	@Override
	public String toString() {
		return "ElectronicDevice [dID=" + dID + ", dName=" + dName + ", dPrice=" + dPrice + ", dColor=" + dColor + "]";
	}
	
}
	