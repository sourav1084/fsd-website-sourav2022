package ArrayLists;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayLists{
	
	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		//Add Element
		
		list.add(0);
		list.add(1);
		list.add(2);
		list.add(3);
		System.out.println("Adding Element  :"+list);
		
		//Get Element
		
		int element = list.get(2);
		System.out.println("Getting Element :"+element);
		
		//Add Element In Between ArrayList
		
		list.add(0, 2);
		System.out.println("Add Ele in Between :"+list);
		
		//Set Element in ArrayList
		
		list.set(1, 9);
		System.out.println("Set Ele At Index   :"+list);
		
		//Delete Element From List
		
		list.remove(4);
		System.out.println("Remove Ele From List :"+list);
		
		//Size Of List

		int size = list.size();
		System.out.println("Getting Size Of List :"+size);
		
		//Loops in ArrayList

		System.out.println("Showing Elements Below:-");
		for(int i =0;i<list.size();i++) {
			System.out.print(list.get(i)+",");
		}
		System.out.println();
	
		//Sorting in ArrayList
		
		Collections.sort(list);
		System.out.println("Sorting ArrayList :"+list);
		
		//
		
		
		
		
	}
}