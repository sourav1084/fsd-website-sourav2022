package Interview;

public class V_Method {

	public static void main(String args[]) {

		Vichel obj = new Vichel();// creating object
		obj.run();// calling method
		System.out.println(obj.instanceParent);

//		Set mySet = new TreeSet<>(); //Example
		Vichel obj1 = new V_Bike();
		obj1.run();
		System.out.println(obj1.instanceParent);

	}
}
