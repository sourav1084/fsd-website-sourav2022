package Interview;

import java.util.HashMap;
import java.util.Map;

public class Iteration {

	public static void main(String[] args) {

		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("Sourav", 101);
		map.put("Raju", 102);
		map.put("James", 103);
		map.put("Shakil", 104);
		map.put("Rubul", 105);

		// for each loop

		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String key = (String) entry.getKey();

			Integer value = entry.getValue();

			System.out.println(key + " " + value);

		}
	}
}
