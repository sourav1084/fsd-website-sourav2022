package Interview;

import java.util.HashMap;

public class RemoveDup {

	public static void remove(int arr[]) {

		HashMap<Integer, Boolean> map = new HashMap<Integer, Boolean>();

		for (int i = 0; i < arr.length; i++) {

			if (map.get(arr[i]) == null) {

				System.out.println(arr[i]);

				map.put(arr[i], true);
			}
		}

	}

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5, 3, 4, 5, 6, 7, 8, 9 };

		remove(arr);
	}

}
