package Interview;
public class  P{  
   void PrintData() {  
      System.out.println("method of parent class");  
   }  
}  
  
class Child extends P {  
   void PrintData() {  
      System.out.println("method of child class");  
   }  
}

class D{
   public static void main(String args[]) {  
        
      P obj1 = (P) new Child();  
      P obj2 = (P) new Child();   
      obj1.PrintData();  
      obj2.PrintData();  
   
}
}