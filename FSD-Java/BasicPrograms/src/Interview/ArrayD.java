package Interview;

import java.util.TreeSet;

public class ArrayD {

	public static void Duplicates(int[] arr) {

		TreeSet<Integer> set = new TreeSet<Integer>();

		for (int i = 0; i < arr.length; i++) {

		 set.add(arr[i]);

		}
		System.out.print(set);
	}

	public static void main(String[] args) {

		int[] arr = { 1, 2, 3, 4, 5, 3, 4, 5, 6, 7, 8 };

		Duplicates(arr);
	}

}
