package Interview;

//package Collections;

import java.util.ArrayList;
import java.util.Collections;

/*
 * 4. Create a Person class with pId, personName and personage as parameters and write a program tp 
      short 4 person objects in below criteria 
    i. ascending order of their age 
   ii. Ascending order of Name 
  iii. Descending Order of pId  
 */
public class Student5 {

	private int pId;
	private String pName;
	private int pAge;

	public Student5() {
	}

	public Student5(int pId, String pName, int pAge) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.pAge = pAge;
	}

	public int getpId() {
		
		return pId;
	}

	public void setpId(int pId) {
//		this.pId = pId;
		System.out.println(this.pId=pId);
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public int getpAge() {
		return pAge;
	}

	public void setpAge(int pAge) {
		this.pAge = pAge;
	}

	@Override
	public String toString() {
		return "Student [pId=" + pId + ", pName=" + pName + ", pAge=" + pAge + "]";
	}

	public static void main(String[] args) {
		Student5 p = new Student5();
		p.setpName("golu");
		p.setpId(1001);
		p.setpAge(99);
		Student5 p1 = new Student5(1000, "molu", 999);
		Student5 p2 = new Student5(1007, "dholu", 9);
		Student5 p3 = new Student5(1003, "bholu", 88);
		System.out.println(p);
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);

		System.out.println();

		/* Ascending order of their Age */

		System.out.println("Ascending order of their Age : ");

		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(p.getpAge());
		list.add(p1.getpAge());
		list.add(p2.getpAge());
		list.add(p3.getpAge());

		System.out.println("Before :" + list);

		Collections.sort(list);

		System.out.println("After :" + list);

		System.out.println();
		
		/* Ascending order of their Name */

		System.out.println("Ascending order of their Name : ");

		ArrayList<String> list2 = new ArrayList<String>();
		list2.add(p.getpName());
		list2.add(p1.getpName());
		list2.add(p2.getpName());
		list2.add(p3.getpName());

		System.out.println("Before :" + list2);

		Collections.sort(list2);

		System.out.println("After :" + list2);

		System.out.println();

		/* Descending order of their ID */

		System.out.println("Descending order of their ID : ");

		ArrayList<Integer> list1 = new ArrayList<Integer>();
		list1.add(p.getpId());
		list1.add(p1.getpId());
		list1.add(p2.getpId());
		list1.add(p3.getpId());

		System.out.println("Before :" + list1);

		Collections.sort(list1, Collections.reverseOrder());

		System.out.print("After :" + list1);
	}

}
