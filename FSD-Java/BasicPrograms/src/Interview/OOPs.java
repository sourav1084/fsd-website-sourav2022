package Interview;

class Student {

	String name;
	static String school;

	static void changeSchool() {

		school = "RJIT";
		System.out.println(school);
	}
}

public class OOPs {

	public static void main(String[] args) {

		Student.school = "KVS";
		System.out.println(Student.school);

		Student s = new Student();
		s.school = "James";
		System.out.println(s.name);

		s.changeSchool();
	}

}
