package Interview;

public class DeleteDupli {

	public static void duplicate(int arr[], int size) {

		for (int i = 0; i < size; i++) {

			boolean duplicate = false;
			
			int count = 0;
			while (count < i) {
				if (arr[i] == arr[count])
					duplicate = true;
				count++;
			}
			if (duplicate == false)
				System.out.print(arr[i] + " ");
		}
	}

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5, 3, 4, 5, 6, 7, 8, 9 }; 
		int size = arr.length;

		duplicate(arr, size);
		
	}
}
