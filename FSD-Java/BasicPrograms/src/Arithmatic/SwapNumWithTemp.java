package Arithmatic;

public class SwapNumWithTemp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 10;
		int num2=20;

		System.out.println("Before swap: num1 = "+num1+" and\n             num2 = "+num2);
       int tempnum=num1;
       num1 =num2;
       num2=tempnum;
       System.out.println("After swap: num1 = "+num1+" and\n             num2 = "+num2);
	}

}
