package GitPrograms;

public class Operations {

	public static void main(String[] args) {
		
		/*Write a Java program to print the result of the following operations. Go to the editor
		Test Data:
		a. -5 + 8 * 6
		b. (55+9) % 9
		c. 20 + -3*5 / 8
		d. 5 + 15 / 3 * 2 - 8 % 3*/
		
		System.out.println("solution = "+(-5+8*6));
		System.out.println("solution = "+((55+9) % 9));
		System.out.println("solution = "+(20 + -3*5 / 8));
		System.out.println("solution = "+(5 + 15 / 3 * 2 - 8 % 3));

	}

}
