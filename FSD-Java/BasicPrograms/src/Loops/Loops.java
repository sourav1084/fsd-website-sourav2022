package Loops;
import java.util.*;
public class Loops{
	
	public static void main (String args[]) {
		/*Write a Java program that takes a number as input and prints its multiplication table upto 10.*/
		
		
		Scanner sc =  new Scanner (System.in);
		 System.out.println("Enter Number = ");
		 int n = sc.nextInt();
		 
		 for (int i = 0; i<10; i++) {
			System.out.println(n + " x " + (i+1) + " = " + (n * (i+1)));
		 }
		 
		
	}
}