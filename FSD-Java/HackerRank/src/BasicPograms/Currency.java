package BasicPograms;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Currency {
//	Given a double-precision number,payment,
//	denoting an amount of money, 
//	use the NumberFormat class' 
//	getCurrencyInstance method to convert  
//	into the US, Indian, Chinese, and French 
//	currency formats.
//	Then print the formatted values as follows:
	    
	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.println("Enter Input :- ");
	        double payment = scanner.nextDouble();
	        scanner.close();

	        // Write your code here.
	        NumberFormat n1 = NumberFormat.getCurrencyInstance(Locale.US);
	        String us = n1.format(payment);
	        
	        NumberFormat n2 = NumberFormat.getCurrencyInstance(new Locale("en","IN")); 
	        String India = n2.format(payment);
	        
	        NumberFormat n3 = NumberFormat.getCurrencyInstance(Locale.CHINA);
	        String China = n3.format(payment);
	        
	        NumberFormat n4 = NumberFormat.getCurrencyInstance(Locale.FRANCE);
	        String France = n4.format(payment);
	         
	        System.out.println("US: " + us);
	        System.out.println("India: " + India);
	        System.out.println("China: " + China);
	        System.out.println("France: " + France);
	    }
	}

