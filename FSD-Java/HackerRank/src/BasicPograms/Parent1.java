package BasicPograms;

public class Parent1 {
	void PrintData() {
		System.out.println("method of parent class");
	}
}

class Child extends Parent1 {
	void PrintData() {
		System.out.println("method of child class");
	}
}

class UpcastingExample {
	public static void main(String args[]) {

		Parent1 obj1 = (Parent1) new Child();
		Parent1 obj2 = (Parent1) new Child();
		obj1.PrintData();
		obj2.PrintData();
	}
}